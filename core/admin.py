from django.contrib import admin

# Register your models here.
from django.urls import reverse
from django.utils.safestring import mark_safe


def get_admin_list_link(item, id="", label=""):
    url = reverse("admin:%s_%s_changelist" % (item._meta.app_label, item._meta.model_name))
    return mark_safe('<a href="{}?q={}">{}</a>'.format(url, id, label))
