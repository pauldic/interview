import os, random
from django.conf import settings
from django.db import models
from django.utils.translation import ugettext_lazy as _

auto_manage_db = True


class Country(models.Model):
    code = models.CharField(_("Code"), max_length=3, primary_key=True)
    name = models.CharField(_("Name"), max_length=64)
    name_plain = models.CharField(_("ASCII Name"), max_length=64)
    is_active = models.BooleanField(_("Is Active"), default=False)
    dial_code = models.CharField(_("Dial Code"), max_length=4, blank=True, null=True)
    flag = models.CharField(_("Flag"), max_length=4, blank=True, null=True)
    capital = models.CharField(_("Capital"), max_length=64, blank=True, null=True)
    region = models.CharField(_("Region"), max_length=64, blank=True, null=True)
    region_des = models.CharField(_("Region description"), max_length=64, blank=True, null=True)

    def __str__(self):
        return self.name or u''

    def __unicode__(self):
        return self.name or u''

    class Meta:
        ordering = ('name',)
        managed = auto_manage_db
        verbose_name = _('Country')
        verbose_name_plural = _('Countries')


def rename_document_file(instance, filename):
    # TODO: Format filename to include Version eg: abc.pdf should be renamed to abc_v1.pdf
    ext = filename.split('.')[-1]
    if instance:
        filename = '{}_{}.{}'.format(instance.title, "", ext)
    # return os.path.join(settings.DOCUMENT_UPLOAD_PATH % (timezone.now().strftime("%Y/%m/%d/")), filename)
    if os.path.isfile(os.path.join(settings.MEDIA_ROOT, settings.DOCUMENT_UPLOAD_PATH, instance.owner_user.username, filename)):
        filename = "{}_{}.{}" % (filename.split(".")[0], random.randint(1000, 9999), filename.split(".")[1])
    return os.path.join(settings.DOCUMENT_UPLOAD_PATH, instance.owner_user.username, "/", filename)


class Document(models.Model):
    owner_user = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE, verbose_name=_("Owner"), related_name="owner")
    title = models.CharField(_("Title"), max_length=45)
    file = models.FileField(_("File"), upload_to=rename_document_file)
    mime_type = models.CharField(_("Mime Type"), max_length=255, blank=True, null=True)
    size = models.IntegerField(_("Size"), )
    created = models.DateTimeField(_("Created"), auto_now_add=True)
    reviewer_user = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE, verbose_name=_("Reviewer"), related_name="reviewer")
    reviewed_file = models.FileField(_("Reviewed File"), max_length=255, blank=True, null=True, default=None)
    review_date = models.DateTimeField(_("Review Date"), null=True, blank=True, default=None)
    module = models.ForeignKey('Module', on_delete=models.CASCADE, verbose_name=_("Module"))
    programme = models.ForeignKey('Programme', on_delete=models.CASCADE, verbose_name=_("Programme"))

    def __unicode__(self):
        return self.file.name

    class Meta:
        managed = auto_manage_db
        ordering = ('-created',)
        verbose_name = _('Document')
        verbose_name_plural = _('Document')


class Industry(models.Model):
    name = models.CharField(_("Name"), max_length=45)
    description = models.CharField(_("Description"), max_length=255, blank=True, null=True)

    def __unicode__(self):
        return self.name

    class Meta:
        managed = auto_manage_db
        ordering = ('name',)
        verbose_name = _('Industry')
        verbose_name_plural = _('Industries')


class Module(models.Model):
    name = models.CharField(_("Name"), max_length=45)
    type = models.CharField(_("Type"), max_length=45)
    description = models.CharField(_("Description"), max_length=255, blank=True, null=True)

    def __unicode__(self):
        return self.name

    class Meta:
        managed = auto_manage_db
        ordering = ('name',)
        verbose_name = _('Module')
        verbose_name_plural = _('Modules')


class PairingRequest(models.Model):
    user = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE, verbose_name=_("User"))
    rating_score = models.IntegerField(_("Rating"), )
    start_date = models.DateTimeField(_("Start"), blank=True, null=True)
    created = models.DateTimeField(_("Created"), blank=True, null=True)
    pairing_request = models.CharField(_("Request"), max_length=45, blank=True, null=True)
    comment = models.CharField(_("Comment"), max_length=255, blank=True, null=True)
    time_slots = models.ManyToManyField('TimeSlot', default=None)
    industries = models.ManyToManyField('Industry', default=None)

    def __unicode__(self):
        return self.pairing_request

    class Meta:
        managed = auto_manage_db
        db_table = "core_pairing_request"
        ordering = ('-created',)
        verbose_name = _('Pairing Request')
        verbose_name_plural = _('Pairing Requests')


class PairedUser(models.Model):
    first_user = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE, verbose_name=_("User"), related_name="first")
    second_user = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE, verbose_name=_("User"), related_name="second")
    programme = models.ForeignKey('Programme', verbose_name=_("Rating"), on_delete=models.CASCADE, )
    step = models.IntegerField(_("Step"), )
    completed = models.BooleanField(_("Completed"), default=False)
    created = models.DateTimeField(_("Created"), blank=True, null=True)
    com = models.CharField(_("Request"), max_length=45, blank=True, null=True)
    comment = models.CharField(_("Comment"), max_length=255, blank=True, null=True)

    def __unicode__(self):
        return self.first_user.first_name

    class Meta:
        managed = auto_manage_db
        db_table = "core_paired_user"
        ordering = ('-created',)
        verbose_name = _('Module')
        verbose_name_plural = _('Modules')


class Profile(models.Model):
    # ACTIVE = 'Active'; SUSPENDED = 'Suspended'; PAUSED = 'Paused';
    # STATUS = ((ACTIVE, _('Active')), (SUSPENDED, _('Suspended')), (PAUSED, _('Paused')))

    user = models.OneToOneField(settings.AUTH_USER_MODEL, verbose_name=_("User"), on_delete=models.CASCADE)
    phone = models.CharField(_("Phone"), max_length=11, null=False, blank=False)
    country = models.ForeignKey(Country, models.DO_NOTHING, verbose_name=_("Country"), default=52)
    created = models.DateTimeField(_("Created"), auto_now_add=True)
    # status = models.CharField(max_length=14, default=ACTIVE, choices=STATUS)

    def __unicode__(self):
        return self.user.first_name

    class Meta:
        managed = auto_manage_db
        ordering = ('created',)
        verbose_name = _('Profile')
        verbose_name_plural = _('Profiles')


class Programme(models.Model):
    name = models.CharField(_("Name"), max_length=45, blank=True, null=True)
    description = models.CharField(_("Description"), max_length=255, blank=True, null=True)
    duration = models.IntegerField(_("Duration"), blank=True, null=True)
    created = models.DateTimeField(_("Created"), auto_now_add=True)

    def __unicode__(self):
        return self.user.name

    class Meta:
        managed = auto_manage_db
        ordering = ('name',)
        verbose_name = _('Programme')
        verbose_name_plural = _('Programmes')


class ProgrammeModule(models.Model):
    programme = models.ForeignKey(Programme, verbose_name=_("Programme"))
    module = models.ForeignKey(Module, verbose_name=_("Module"))
    sequence_number = models.IntegerField(_("Sequence"))

    def __unicode__(self):
        return self.programme.name

    class Meta:
        managed = auto_manage_db
        db_table = "core_programme_module"
        ordering = ('programme', 'sequence_number',)
        verbose_name = _('Programme Module')
        verbose_name_plural = _('Programme Modules')


class RatingAnswer(models.Model):
    question = models.ForeignKey('RatingQuestion', verbose_name=_("Question"))
    answer = models.CharField(_("Answer"), max_length=255)
    order = models.IntegerField(_("Order"), blank=True, null=True)

    def __unicode__(self):
        return self.answer

    class Meta:
        managed = auto_manage_db
        db_table = "core_rating_answer"
        ordering = ('question', 'order',)
        verbose_name = _('Rating Answer')
        verbose_name_plural = _('Rating Answers')


class RatingQuestion(models.Model):
    question = models.CharField(_("Question"), max_length=255)
    type = models.CharField(_("Type"), max_length=45)

    def __unicode__(self):
        return self.question

    class Meta:
        managed = auto_manage_db
        db_table = "core_rating_question"
        ordering = ('type', 'question')
        verbose_name = _('Rating Question')
        verbose_name_plural = _('Rating Questions')


class TimeSlot(models.Model):
    label = models.CharField(_("Label"), max_length=45)
    start = models.DateTimeField(_("Start"), )
    end = models.DateTimeField(_("End"), blank=True, null=True, default=None)

    def __unicode__(self):
        return self.label

    class Meta:
        managed = auto_manage_db
        db_table = "core_time_slot"
        ordering = ('label',)
        verbose_name = _('Module')
        verbose_name_plural = _('Modules')


class UniversityProgramme(models.Model):
    programme_name = models.CharField(max_length=45)
    institution = models.CharField(max_length=45, blank=True, null=True)
    year = models.CharField(max_length=4, blank=True, null=True)
    users = models.ManyToManyField(settings.AUTH_USER_MODEL, default=None)

    def __unicode__(self):
        return self.programme_name

    class Meta:
        managed = auto_manage_db
        db_table = "core_university_programme"
        ordering = ('programme_name',)
        verbose_name = _('University Programme')
        verbose_name_plural = _('University Programmes')

